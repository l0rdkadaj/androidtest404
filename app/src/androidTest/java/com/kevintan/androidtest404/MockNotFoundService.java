package com.kevintan.androidtest404;

import android.util.Log;

import com.google.gson.Gson;

import io.reactivex.Observable;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.Calls;

public class MockNotFoundService implements ExampleApi{

    private static final String TAG = "MockNotFoundService";

    private final BehaviorDelegate<ExampleApi> delegate;

    public MockNotFoundService(BehaviorDelegate<ExampleApi> delegate) {
        this.delegate = delegate;
    }

    @Override
    public Observable<ExampleResponse> getExample() {
        // simulate a 404 not found
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setError(new Error(404, "Not found"));
        String json  = new Gson().toJson(errorResponse);
        Response<ErrorResponse> response = Response.error(404, ResponseBody.create(MediaType.parse("application/json") ,json));
        return delegate.returning(Calls.response(response)).getExample();
    }
}
