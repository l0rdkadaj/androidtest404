package com.kevintan.androidtest404;

import android.support.test.filters.SmallTest;
import android.test.InstrumentationTestCase;

import org.junit.Assert;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.mock.BehaviorDelegate;
import retrofit2.mock.MockRetrofit;
import retrofit2.mock.NetworkBehavior;

public class ExampleTest extends InstrumentationTestCase {

    private MockRetrofit mockRetrofit;

    private ExampleResponse exampleResponse;

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        Retrofit retrofit = new Retrofit.Builder().baseUrl("http://test.com")
                .client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        NetworkBehavior behavior = NetworkBehavior.create();

        mockRetrofit = new MockRetrofit.Builder(retrofit)
                .networkBehavior(behavior)
                .build();
    }

    @SmallTest
    public void testNotFound() {
        BehaviorDelegate<ExampleApi> delegate = mockRetrofit.create(ExampleApi.class);
        MockNotFoundService mockNotFoundService = new MockNotFoundService(delegate);

        mockNotFoundService.getExample().subscribe(exampleResponse -> {
            this.exampleResponse = exampleResponse;
        }, throwable -> {
            Assert.assertFalse(this.exampleResponse != null);
            Assert.assertEquals("not found", throwable.getMessage());
        });
    }
}
