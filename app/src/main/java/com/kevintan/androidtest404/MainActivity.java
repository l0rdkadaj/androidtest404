package com.kevintan.androidtest404;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Retrofit retrofit = new Retrofit.Builder().baseUrl("http://test.com")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ExampleApi exampleApi = retrofit.create(ExampleApi.class);

        exampleApi.getExample().subscribe(exampleResponse -> {
            //this will definitely get the response
            Log.d(TAG, "response received");
        }, throwable -> {
            //but what if http error 404 happens?
            Log.e(TAG, "failed to get response "+throwable.getMessage());
        });
    }
}
