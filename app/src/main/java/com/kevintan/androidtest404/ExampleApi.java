package com.kevintan.androidtest404;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface ExampleApi {
    @GET(value = "/api/miner_newd/")
    Observable<ExampleResponse> getExample();
}
