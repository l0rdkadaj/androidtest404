package com.kevintan.androidtest404;

public class ExampleResponse {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ExampleResponse{" +
                "message='" + message + '\'' +
                '}';
    }
}
