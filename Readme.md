**Answer to Question 1:**

Since there is no silver bullet way of doing testing, the most appropriate way of testing error 404
scenarios is by using Retrofit mock services and Android's built in testing framework. Since Retrofit
is used to call the APIs, we can easily create a mock API implementation that helps us simulate 
failure case scenarios.

Though I have opinions that writing the test case is a bit verbose and not so readable, 
but this works for now. 
(I was a bit too used to Spock unit testing for web application development testing)

**Answer to Question 2:**

I've done quite a lot of apps before, ranging from simple ones that I've done for a company that
does selfies with santa picture overlays that got us 100k downloads in a few years, to something
that is really polished by a designer such as Shoppr.

The best that I should highlight that really got me excited working on was Shoppr, formerly known
for Tinder for Fashion, Instagram for Outfits. I've worked on the project from v0 to
v1 for 6 months because of requirements that changes most of the time. Their components for the 
mobile app are mostly related to Tinder style card swiping, an endless list that has pictures of
fashion influencers with likes, comments, and follows.

Then from v1-v1.1, I have to delegate work to a Russian team which was believed to be good at what they're doing.

For Shoppr, I've handled parts such as social login for Facebook and Instagram, to card swiping, to the clothing
details page, to saving a particular cloth item to a wishlist, to linking to their respective external 
e-commerce site for them to make a decision to purchase items from there.

I can produce products like that when a designer who is good in motion graphics design is involved with me in
Android development. It helps me visualize and plan ahead for better user experiences.

Aside from Android development, I can do web development in the sense that I can do domain driven 
designs and spin off APIs fairly quickly with the right JVM web development frameworks. Since mobile
development is hugely dependent on the backend in order to consume data, I can design the API from
the top down in order to minimize the number of API calls being done in one screen.

How do I judge code quality? Code quality can sometimes be subjective because it can improve over 
time after several development iterations. If we're not constrained by time, then we can implement
different types of design patterns such as Model View Presenter or Model View ViewModel so that we 
can automate tests at different parts of the application, ranging from API to automating UI tests,
as well as to test the application business logic. In the minimum, one should at least have proper
abstraction layers (even something very bare minimum such as interfaces, not a fan of abstract classes yet) 
in place so that we can do rewrites or just simulate different use case scenarios when no test server 
is being used in the beginning stage of development.